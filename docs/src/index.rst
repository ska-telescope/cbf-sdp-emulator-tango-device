.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


==============================
CBF-SDP Emulator TANGO Devices
==============================

.. toctree::
  device
  api

This project implements a Tango device that wraps
the :doc:`cbf-sdp-emulator:index`.

Several aspects of the sending process can be adjusted via Tango attributes.
This includes the Measurement Set to send, target host/port, data rate
and others.
In the future the way this configuration takes place might change
to accommodate with the rest of the architecture.

The repository structure is based on the `tango-example
<https://github.com/ska-telescope/tango-example>`_ project.
However, certain things have been simplified
to ease local development without the need to run everything inside containers.
