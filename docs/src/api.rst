.. doctest-skip-all
.. _package-guide:


API
===

.. automodule:: ska.cbf.tango_device

.. autoclass:: ska.cbf.tango_device.CbfSubarray
    :members:
