import asyncio
import contextlib
import json
import os
import shutil
import socket
import tarfile
import tempfile
import threading
import time
import urllib

from cbf_sdp import packetiser
from ska_tango_base.commands import ResultCode
from ska_tango_base.csp_subelement_subarray import CspSubElementSubarray
from ska_tango_base.subarray_device import SKASubarrayResourceManager
import tango
from tango.server import attribute, command


DEFAULT_MS_REPO = 'https://gitlab.com/ska-telescope/cbf-sdp-emulator/-/raw/master/data'
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11'
EXPECTED_CONFIGURATION_DELAY = 180


def url_open(url):
    """
    Open a URL respecting proxy environment variables (`*_proxy`) and using a
    browser-like user agent to avoid user-agent-based request denials.
    """
    request = urllib.request.Request(url, headers={'User-Agent': USER_AGENT})
    return urllib.request.urlopen(request)


class SimpleResourceManager(SKASubarrayResourceManager):
    """
    Specialisation of SKASubarrayResourceManager that doesn't require JSON
    contents to be keyed under 'example' but simply iterables.
    """

    def assign(self, resources):
        self._resources |= set(resources)

    def release(self, resources):
        self._resources -= set(resources)


class Base(CspSubElementSubarray):
    """
    Class bridging differences between CspSubElementSubarray and CbfSubarray

    There are two main differences between CspSubElementSubarray (and its base
    SKASubarray) and CbfSubarray:

     * The former transition from EMPTY to RESOURCING and to IDLE via the
       AssignResources Tango command taking a string. In CbfSubarray this
       happens via the AddReceptors command taking a list of ints. Since the
       state transition taking place is the same in both cases though, both
       classes implement their commands via the AssignResourcesCommand class,
       which is meant to receive a Resource Manager object instead of the Device
       object itself during construction.
     * CbfSubarray offers an attribute called "receptors" corresponding to the
       receptors added via AddReceptors.
     * SKASubarray defines a Scan command taking a string, but CbfSubarray
       re-defines this command taking an int.

     To bridge these gaps, this class:

      * Sets up a SimpleResourceManager object (see above) to hold "receptors".
        This done by overriding the InitCommand, which is where SKASubarray sets
        up its reource manager.
      * Defines an AddReceptors command taking a list of ints, and invokes the
        default AssignResourcesCommand object, which in turn will use the
        SimpleResourceManager to register these receptors.
      * Defines a read-only receptors attribute that reads the values from the
        SimpleResourceManager
      * Defines a Scan command taking an integer value instead of a string,
        which then invokes the ScanCommand object.
      * Overrides the ScanCommand class's do method to avoid the original class
        from interpreting the input as a string.
    """

    # We override the InitCommand to setup a simpler Resource Manager
    # That way the original AssignResourcesCommand code works correctly
    # and we don't need to override *that*
    class InitCommand(CspSubElementSubarray.InitCommand):
        def do(self):
            code, msg = super().do()
            if code not in (ResultCode.STARTED, ResultCode.OK):
                return code, msg
            self.target.resource_manager = SimpleResourceManager()
            return code, msg

    # The "receptors" attribute is used by CspSubarray after AddResources finishes
    # and is therefore required.
    # We wanted to have it as a read-only attribute, but we hit a pytango bug:
    # https://gitlab.com/tango-controls/pytango/-/issues/230
    receptors = attribute(
        dtype=('uint16',),
        access=tango.AttrWriteType.READ_WRITE,
        max_dim_x=197,
        label="Receptors",
        doc="List of receptors assigned to subarray",
    )

    def read_receptors(self):
        return list(self.resource_manager.get())

    def write_receptors(self, r):
        pass

    # AddReceptors is called by CspSubarray during its AddResources
    # AddResources receives a string, but AddReceptors receives a list of ints
    @command(
        dtype_in=('uint16',),
        doc_in="List of receptor IDs",
        dtype_out='DevVarLongStringArray',
        doc_out="(ReturnType, 'informational message')"
    )
    def AddReceptors(self, argin):
        """
        Assign Receptors to this subarray.
        Turn subarray to ObsState = IDLE if previously no receptor is assigned.
        """
        command = self.get_command_object("AssignResources")
        (return_code, message) = command(argin)
        return [[return_code], [message]]

    # RemoveReceptors is called by CspSubarray during its ReleaseResources
    # ReleaseResources receives a string, but RemoveReceptors receives a list of ints
    @command(
        dtype_in=('uint16',),
        doc_in="List of receptor IDs",
        dtype_out='DevVarLongStringArray',
        doc_out="(ReturnType, 'informational message')"
    )
    def RemoveReceptors(self, argin):
        """
        Remove Receptors from this subarray.
        Turn subarray to ObsState = IDLE if previously no receptor is assigned.
        """
        command = self.get_command_object("ReleaseResources")
        (return_code, message) = command(argin)
        return [[return_code], [message]]

    # Scan command needs re-definition to take an int instead of a string
    @command(
        dtype_in='uint',
        doc_in="Scan ID",
        dtype_out='DevVarLongStringArray',
        doc_out="(ReturnType, 'informational message')",
    )
    def Scan(self, argin):
        """
        Start Scan
        """
        command = self.get_command_object("Scan")
        (return_code, message) = command(argin)
        return [[return_code], [message]]

    # The base ScanCommand class interprets argin as a string, which we need to
    # avoid
    class ScanCommand(CspSubElementSubarray.ScanCommand):
        def do(self, argin):
            return ResultCode.OK, 'Scan started'


    # When building the documentation, we choose to mock our dependencies
    # instead of needing to install them, as this would take a long time,
    # and can even require compiling packages.
    # However, Sphinx's autodoc_mock_imports mechanism doesn't deal well
    # with hierarchies. For example, the base class for
    # CbfSubarray.AbortCommand (Base.AbortCommand) doesn't exist in this
    # Base type, but in CspSubElementSubarray. but autodoc doesn't realise
    # about this. The same applies to the other command classes.
    # By explicitly declaring these classes on the Base type we give
    # autodoc a chance to successfully resolve all names; otherwise
    # these assignments are unnecessary.
    # See https://github.com/sphinx-doc/sphinx/issues/9168 for details.
    ConfigureScanCommand = CspSubElementSubarray.ConfigureScanCommand
    AbortCommand = CspSubElementSubarray.AbortCommand
    EndScanCommand = CspSubElementSubarray.EndScanCommand

class CbfSubarray(Base):
    """
    A Tango device to control the CBF-SDP interface emulator sender.

    This tango device has the exact same interface as Mid-CBF's CbfSubarray, as
    it's meant to be a drop-in replacement for the latter. To ease this use case
    and avoid having to write different contents on the Tango DB depending on
    whether the "real" CbfSubarray device or this one is deployed, this device's
    class name follows that of Mid-CBF's.
    """

    def __init__(self, cl, name):
        self._transmission_target_ip = "127.0.0.1"
        self._transmission_target_port = 41000
        self._transmission_source = "sim-vis.ms"
        self._transmission_source_url = None
        self._transmission_num_channels = 1
        self._transmission_rate = 14750
        self._transmission_channels_per_stream = 0
        self._loop = None
        self._sender_task = None
        self._sender_thread = None
        self._sending = False
        self._configuration_thread = None
        self._name_resolution_max_tries = 100
        self._name_resolution_retry_period = 0.2
        self._time_interval = 0
        self._reader_num_repeats = 1
        self._reader_num_timestamps = 0

        super().__init__(cl, name)

    def delete_device(self):
        super().delete_device()
        self._join_configuration_thread()
        self._stop_packetiser()

    def _retrieve_transmission_source(self):
        url = self._transmission_source_url
        ms_name = os.path.basename(self._transmission_source)
        target_dir = os.path.dirname(self._transmission_source) or '.'
        if not url:
            tarball_name = ms_name + ".tar.gz"
            url = DEFAULT_MS_REPO + '/' + tarball_name

        # Retrieve tarball and extract it
        self.info_stream("Downloading %s", url)
        tarball = tempfile.mktemp()
        with open(tarball, 'wb') as wfile, url_open(url) as rfile:
            shutil.copyfileobj(rfile, wfile)
        self.info_stream("Extracting %s into %s", url, target_dir)
        with contextlib.closing(tarfile.open(tarball)) as tar:
            tar.extractall(path=target_dir)

        if not os.path.exists(self._transmission_source):
            self.error_stream("After retrieval, data was not where it ought to be :(")
            raise RuntimeError

    def _join_configuration_thread(self):
        if self._configuration_thread:
            self._configuration_thread.join()
            self._configuration_thread = None

    def _resolve_output_host(self):
        tries = 0
        hostname = self._transmission_target_ip
        while tries < self._name_resolution_max_tries:
            try:
                ip = socket.gethostbyname(hostname)
            except socket.gaierror as e:
                name_resolution_error = e
                tries += 1
                self.warn_stream("Failed to resolved receiver name %s (try %d/%d)", hostname, tries, self._name_resolution_max_tries)
                time.sleep(self._name_resolution_retry_period)
                continue
            else:
                self.info_stream("Resolved receiver name %s to %s", hostname, ip)
                self._transmission_target_ip = ip
                return
        raise name_resolution_error

    def _apply_config(self, config):
        def extract_field(name, typ, required=False, member_name=None):
            member_name = member_name or "_" + name
            if name in config:
                setattr(self, member_name, typ(config.pop(name)))
            elif required:
                self.warn_stream('configuration field %s missing, using default value', name)

        # See https://confluence.skatelescope.org/display/SE/SPO-944+implementation+notes#SPO-944implementationnotes-CBFConfigureJSON
        # for an example JSON string
        #
        # TODO (rtobar):
        #
        # This Tango device supports launching a single emulator sender, which
        # in turn supports sending data for different, consecutive chunks of
        # channels read from a single MS to as many receivers, all on the same
        # host, and listening in consecutive ports. This model might not adjust
        # well to what might eventually be needed from this Tango device though,
        # where receivers could be in arbitrary endpoints. Changes in the
        # emulator sender are required to suppor this.
        #
        # For the time being we are adapting to the current emulator sender
        # behavior, configuring it to send to a single receiver (the first on
        # the list of receivers)

        config = config['fsp'][0]
        extract_field('outputHost', lambda v: v[0][1], True, '_transmission_target_ip')
        extract_field('outputPort', lambda v: int(v[0][1]), True, '_transmission_target_port')
        extract_field('transmission_source', str, True)
        extract_field("integrationTime", int, False, '_time_interval')

        # TODO (rtobar):
        # Check how to set these, specially num_channels and rate
        # * transmission_source_url
        # * transmission_num_channels
        # * transmission_rate
        # * Any others?
        # For the time being we support them as extensions
        extract_field("transmission_source_url", str)
        extract_field("transmission_num_channels", int)
        extract_field("transmission_channels_per_stream", int)
        extract_field("transmission_rate", int)
        extract_field("reader_num_repeats",int)
        extract_field("reader_num_timestamps",int)
        extract_field("name_resolution_max_tries", int)
        extract_field("name_resolution_retry_period", float)
        if config:
            self.warn_stream(
                "Extra configuration fields not used: %r", list(config.keys()))

    def _assemble_config(self):
        return {
            'transmission': {
                'target_host': self._transmission_target_ip,
                'target_port_start': self._transmission_target_port,
                'rate' : self._transmission_rate,
                'time_interval': self._time_interval / 1000,
                'channels_per_stream': self._transmission_channels_per_stream,
            },
            'reader': {
                'start_chan': 0,
                'num_repeats': self._reader_num_repeats,
                'num_timestamps': self._reader_num_timestamps,
                'num_chan': self._transmission_num_channels,
            }
        }

    def _run_packetiser(self, setup_done_event, scan_command):
        sending = packetiser.packetise(
                self._assemble_config(), self._transmission_source)
        self._loop = asyncio.new_event_loop()
        self._sender_task = self._loop.create_task(sending)
        self._sending = True
        setup_done_event.set()

        self.info_stream("Starting cbf-sdp emulator sender")
        try:
            self._loop.run_until_complete(self._sender_task)
        except asyncio.CancelledError:
            # Received a cancel() call via Abort() command, do not perform a
            # state transition
            self.info_stream("Sending process cancelled by EndScan()/Abort() command")
        except Exception as e:
            self.error_stream("Sending process failed: %s", e)
            scan_command.failed()
        else:
            self.info_stream("Sending process finished successfully")
            scan_command.succeeded()
        finally:
            self._sending = False
            self._loop.run_until_complete(self._loop.shutdown_asyncgens())
            self._loop.close()
            self._loop = None

    def _stop_packetiser(self):

        # If not called after Scan()
        if not self._sender_task:
            return "Sending of data had not started"

        assert self._sender_thread
        assert not self._sender_task.cancelled()

        if self._sender_task.done():
            msg = "Sending of data had already finished"
            self._sender_task = None
            self._sender_thread.join()
            self._sender_thread = None
            return msg

        # TODO: make timeout configurable, revisit error handling
        self.info_stream("Cancelling sending of data")
        self._loop.call_soon_threadsafe(self._sender_task.cancel)
        self._sender_thread.join(timeout=1)
        if self._sender_thread.is_alive():
            self.warn_stream("Sending thread still alive")
        self._sender_task = None
        self._sender_thread = None
        return "Sending of data aborted"

    @attribute(dtype=tango.DevBoolean)
    def sending(self):
        return self._sending

    @attribute(
        dtype='uint16',
        unit="seconds",
        doc="Configuration delay expected in seconds, same as configurationDelayExpected",
    )
    def configureDelayExpected(self):
        return self._config_delay_expected

    # -------------------------------------------------------------------------
    #    CbfSubarray command methods
    # -------------------------------------------------------------------------

    # Our configuration step can take a long time (it automatically
    # downloads potentially big MSs from the internet, and it also loops
    # until hostnames in the k8s domain can be resolved), and we need to
    # communicate CSP about this. This is done via the configureDelayExpected
    # attribute, which is added in this class. The base ObsDevice class
    # however offers a configurationDelayExpected attribute already, and
    # CSP might switch to use that at some point.
    #
    # The "_config_delay_expected" variable (originally defined in
    # ObsDevice) serves these two Tango attributes. If in the future CSP
    # switches to using the configurationDelayExpected attribute we only
    # need to remove our extra attribute.
    #
    # Normally we would have set this attribute in our __init__ method, which is
    # way simpler to maintain, but its value is set to 0 in ObsDevice's
    # InitCommand, so we need to wait until that happens to set a new value
    class InitCommand(Base.InitCommand):
        def do(self):
            code, msg = super().do()
            if code not in (ResultCode.STARTED, ResultCode.OK):
                return code, msg
            assert hasattr(self.target, '_config_delay_expected')
            self.target._config_delay_expected = EXPECTED_CONFIGURATION_DELAY
            return ResultCode.OK, 'Init completed'

    class ConfigureScanCommand(Base.ConfigureScanCommand):

        def configure(self):
            device = self.target
            try:
                if not os.path.exists(device._transmission_source):
                    device._retrieve_transmission_source()
                device._resolve_output_host()
                self.succeeded()
            except Exception as e:
                device.error_stream("Failed to configure device: %s", e)
                self.failed()

        def do(self, argin):
            code, msg = super().do(argin)
            if code not in (ResultCode.STARTED, ResultCode.OK):
                return code, msg
            device = self.target
            device._apply_config(json.loads(argin))
            device._join_configuration_thread()
            device._configuration_thread = threading.Thread(
                target=self.configure)
            device._configuration_thread.start()
            return ResultCode.STARTED, "started configuration"

    class ScanCommand(Base.ScanCommand):

        def do(self, argin):
            """Start the emulator"""
            code, msg = super().do(argin)
            if code not in (ResultCode.STARTED, ResultCode.OK):
                return code, msg
            device = self.target
            device._join_configuration_thread()
            setup_done_event = threading.Event()
            device._sender_thread = threading.Thread(target=device._run_packetiser,
                    name="sender", args=(setup_done_event, self))
            device._sender_thread.start()
            # TODO: revisit error handling
            if not setup_done_event.wait(3):
                device.warn_stream("Sender couldn't start within 3 seconds")
            return ResultCode.STARTED, "emulator started"

    class AbortCommand(Base.AbortCommand):

        def do(self):
            """Stop the emulator"""
            code, msg = super().do()
            if code not in (ResultCode.STARTED, ResultCode.OK):
                return code, msg
            device = self.target
            return ResultCode.OK, device._stop_packetiser()

    class EndScanCommand(Base.EndScanCommand):

        def do(self):
            """Like Abort, it ends the sending"""
            code, msg = super().do()
            if code not in (ResultCode.STARTED, ResultCode.OK):
                return code, msg
            device = self.target
            return ResultCode.OK, device._stop_packetiser()

def main():
    CbfSubarray.run_server()

if __name__ == '__main__':
    main()
