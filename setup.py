#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.rst') as readme_file:
    readme = readme_file.read()
with open('version.txt') as version_file:
    version = version_file.read().strip()

setup(
    name='ska-sdp-cbf-emulator-tango-device',
    version=version,
    description="",
    long_description=readme,
    long_description_content_type='text/x-rst',
    author="Stephen Ord, Rodrigo Tobar",
    author_email='stephen.ord@csiro.au, rtobar@icrar.org',
    url='https://github.com/ska-telescope/cbf-sdp-emulator-tango-device',
    packages=['ska/cbf'],
    include_package_data=True,
    license="BSD license",
    zip_safe=True,
    keywords='TANGO',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    install_requires=[
        'ska-sdp-cbf-emulator >= 1.6',
        'ska-tango-base == 0.10.1',
        'pytango >= 9.3.2',
    ]
)
