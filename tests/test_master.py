# -*- coding: utf-8 -*-
"""
Some simple unit tests of the CBF-SDP Interface Emulator device, exercising the device from
the same host as the tests by using a DeviceTestContext.
"""
import pytest
from pytest_bdd import parsers
from pytest_bdd.scenario import scenarios
from pytest_bdd.steps import given, then
from tango import DevState
from tango.test_context import MultiDeviceTestContext

from ska.cbf.tango_device import CbfSubarray
from ska.cbf.master import CbfMaster


scenarios('master.feature')

NUM_VCC = 4
NUM_FSP = 4

@pytest.fixture
def initialised_devices():
    devices_info = (
        {
            "class": CbfSubarray,
            "devices": [
                {
                    "name": "test/cbf/subarray_1"
                },
            ]
        },
        {
            "class": CbfMaster,
            "devices": [
                {
                    "name": "test/cbf/master",
                    "properties": {
                        "CbfSubarrays": ['tango://127.0.0.1:10000/test/cbf/subarray_1#dbase=no'],
                        "MaxCapabilities": [f"VCC:{NUM_VCC}", f"FSP:{NUM_FSP}"]
                    }
                },
            ]
        }
    )

    with MultiDeviceTestContext(devices_info, host='127.0.0.1', port=10000, process=True, timeout=30) as context:
        master = context.get_device('test/cbf/master')
        subarray = context.get_device('test/cbf/subarray_1')
        yield master, subarray


@given(
    parsers.parse("A Master device in <start_state> state"),
    target_fixture="master"
)
def master(initialised_devices, start_state):
    initialised_master, initialised_subarray = initialised_devices
    assert initialised_master.State() == DevState.STANDBY
    assert initialised_subarray.State() == DevState.OFF

    assert start_state in ('ON', 'OFF', 'STANDBY')
    if start_state == 'ON':
        initialised_master.On()
    elif start_state == 'OFF':
        initialised_master.Off()
    return initialised_master

@then(parsers.parse("It can be moved to <end_state> state"))
def can_be_moved_to(master, initialised_devices, end_state):
    subarray = initialised_devices[1]
    assert end_state in ('ON', 'OFF', 'STANDBY')
    if end_state == 'ON':
        master.On()
        assert master.State() == DevState.ON
        assert subarray.State() == DevState.ON
    elif end_state == 'OFF':
        master.Off()
        assert master.State() == DevState.OFF
        assert subarray.State() == DevState.OFF
    else:
        master.Standby()
        assert master.State() == DevState.STANDBY
        assert subarray.State() == DevState.OFF

@then("Its VCC-receptor mapping can be read")
def vcc_receptor_mapping_can_be_read(master):
    assert len(master.vccToReceptor) == NUM_VCC
    assert len(master.receptorToVcc) == NUM_FSP

@then("Its reporting attributes can be read")
def reporting_attrs_can_be_read(master):
    assert len(master.reportVCCAdminMode) == NUM_VCC
    assert len(master.reportVCCHealthState) == NUM_VCC
    assert len(master.reportVCCState) == NUM_VCC
    assert len(master.reportVCCSubarrayMembership) == NUM_VCC
    assert len(master.reportFSPAdminMode) == NUM_FSP
    assert len(master.reportFSPHealthState) == NUM_FSP
    assert len(master.reportFSPState) == NUM_FSP
    assert len(master.reportFSPSubarrayMembership) == NUM_FSP