CBF-SDP Interface Emulator TANGO Device
=======================================

.. image:: https://readthedocs.org/projects/cbf-sdp-emulator-tango-device/badge/?version=latest
  :target: https://developer.skatelescope.org/projects/cbf-sdp-emulator-tango-device/en/latest/?badge=latest
  :alt: Documentation Status

This project provides a TANGO interface
to the sender portion of the CBF-SDP Interface emulator.
